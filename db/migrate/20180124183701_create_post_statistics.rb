class CreatePostStatistics < ActiveRecord::Migration[5.1]
  def change
    create_table :post_statistics do |t|
      t.references :post
      t.integer :likes
      t.integer :shares
      t.integer :comments
      t.date :collection_date
      t.timestamps
    end
  end
end
