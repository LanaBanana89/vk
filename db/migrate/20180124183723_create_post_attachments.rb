class CreatePostAttachments < ActiveRecord::Migration[5.1]
  def change
    create_table :post_attachments do |t|
      t.references :post
      t.text :url
      t.text :type_content
      t.timestamps
    end
  end
end
