class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.string :gtype
      t.string :name
      t.text :slug
      t.timestamps
    end
  end
end
